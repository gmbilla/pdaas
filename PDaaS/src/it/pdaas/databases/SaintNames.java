package it.pdaas.databases;

public class SaintNames extends Database<String> {
	private static SaintNames instance=null;
	
	private SaintNames(){
		super();
	}
	
	public static synchronized SaintNames getInstance(){
		if(instance == null)
			instance = new SaintNames();
		return instance;
	}
}
