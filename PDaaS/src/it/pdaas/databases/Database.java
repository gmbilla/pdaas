package it.pdaas.databases;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class Database<T> {
	
	private List<T> elements;
	private Random randomGenerator;
	
	public Database(){
		elements = new ArrayList<T>();
	}
	
	public void insert(T el){
		elements.add(el);
	}
	
	public T pickRandom(){
		int choice = randomGenerator.nextInt(elements.size());
		return elements.get(choice);
	}
	
}
