package it.pdaas.generators;

import it.pdaas.blasphemy.Blasphemy;
import it.pdaas.blasphemy.MannaggiaBlasphemy;

import java.util.Random;


public class RandomBlasphemyGenerator {

	private static Blasphemy blasphemies[] = {new MannaggiaBlasphemy()};
	
	public static String getRandomBlasphemy(){
		Random randomGenerator = new Random();
		int choice = randomGenerator.nextInt(blasphemies.length);
		return blasphemies[choice].getRandomBlasphemy();
	}
	
	
}
