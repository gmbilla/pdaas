package it.pdaas.blasphemy;

import it.pdaas.databases.SaintNames;

public class MannaggiaBlasphemy implements Blasphemy {

	@Override
	public String getRandomBlasphemy() {
		String randomSaint = SaintNames.getInstance().pickRandom();
		String blasphemy = "Mannaggia " + randomSaint;
		return blasphemy;
	}

}
